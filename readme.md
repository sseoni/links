### Why?

I read and watch a lot of computer science related content, instead of spamming the links to everyone I know I need a single location to share things from.

### Blogs I've Read

`10 May 2021` **~** [Medium is a poor choice for blogging @ tonsky.me](https://tonsky.me/blog/medium/)

`10 May 2021` **~** [Software disenchantment @ tonsky.me](https://tonsky.me/blog/disenchantment/)

`10 May 2021` **~** [Computers as I used to love them @ tonsky.me](https://tonsky.me/blog/syncthing/)

`10 May 2021` **~** [Dropping cache didn’t drop cache](https://blog.twitter.com/engineering/en_us/topics/open-source/2021/dropping-cache-didnt-drop-cache.html)

`10 May 2021` **~** [How Discord handles push request bursts of over a million per minute with Elixir’s GenStage](https://blog.discord.com/how-discord-handles-push-request-bursts-of-over-a-million-per-minute-with-elixirs-genstage-8f899f0221b4)

`10 May 2021` **~** [Why Discord is switching from Go to Rust](https://blog.discord.com/why-discord-is-switching-from-go-to-rust-a190bbca2b1f)

`10 May 2021` **~** [How Discord Handles Two and Half Million Concurrent Voice Users using WebRTC](https://blog.discord.com/how-discord-handles-two-and-half-million-concurrent-voice-users-using-webrtc-ce01c3187429)

`10 May 2021` **~** [How Discord Resizes 150 Million Images Every Day with Go and C++](https://blog.discord.com/how-discord-resizes-150-million-images-every-day-with-go-and-c-c9e98731c65d)

`10 May 2021` **~** [How Discord Scaled Elixir to 5,000,000 Concurrent Users](https://blog.discord.com/scaling-elixir-f9b8e1e7c29b)

`10 May 2021` **~** [How Discord Indexes Billions of Messages](https://blog.discord.com/how-discord-indexes-billions-of-messages-e3d5e9be866f)

`10 May 2021` **~** [How Discord Stores Billions of Messages](https://blog.discord.com/how-discord-stores-billions-of-messages-7fa6ec7ee4c7)

### Talks I've Enjoyed

`20 May 2021` **~** [The Gleam Programming Language and its Creator Louis Pilfold](https://youtu.be/az_GarVoXEE)

`13 May 2021` **~** [MicroService with go and gRPC](https://youtu.be/dWXedF9ZeUE)

`13 May 2021` **~** [GopherCon 2019: Dave Cheney - Two Go Programs, Three Different Profiling Techniques](https://youtu.be/nok0aYiGiYA)

`5 May 2021` **\~** [Parsing JSON Really Quickly: Lessons Learned](https://www.youtube.com/watch?v=wlvKAT7SZIQ)
